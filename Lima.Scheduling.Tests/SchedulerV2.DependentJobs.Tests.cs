﻿using Lima.Scheduling.Core.V2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Tests {

    [TestClass]
    public class GraphTests {

        /// <summary>
        /// Use case:
        /// -> Job 1.
        /// -> Job 2.
        /// -> Job 3.
        /// </summary>
        [TestMethod]
        public void GetSolutions_all_jobs_independent() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[0], "B", true),
                new Job("3", 5, new string[0], "C", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(0, solution[1].StartTimeSec);
            Assert.AreEqual(0, solution[2].StartTimeSec);

            Assert.AreEqual(5, solution.TotalElapsedTime);
        }

        /// <summary>
        /// Use case: Job 1 -> Job 2 -> Job 3.
        /// </summary>
        [TestMethod]
        public void GetSolutions_all_jobs_sequentially_dependent() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[]{ "1" }, "B", true),
                new Job("3", 5, new string[]{ "2" }, "C", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(1, solution[1].StartTimeSec);
            Assert.AreEqual(4, solution[2].StartTimeSec);

            Assert.AreEqual(9, solution.TotalElapsedTime);
        }

        /// <summary>
        /// Use case: Job 3 depedent on both 1 and 2.
        /// -> Job 1 ->
        ///                 -> Job 3.
        /// -> Job 2 ------>
        /// </summary>
        [TestMethod]
        public void GetSolutions_job_depends_on_multiple() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[0], "B", true),
                new Job("3", 5, new string[]{ "1", "2" }, "C", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(0, solution[1].StartTimeSec);
            Assert.AreEqual(3, solution[2].StartTimeSec);

            Assert.AreEqual(8, solution.TotalElapsedTime);
        }

        /// <summary>
        /// Use case: Job 3 depends only on Job 2.
        /// -> Job 1 -------->
        ///                 
        /// -> Job 2 --> Job 3 ---->
        /// </summary>
        [TestMethod]
        public void GetSolutions_job_depends_on_only_one() {

            var jobs = new[] {

                new Job("1", 5, new string[0], "A", true),
                new Job("2", 1, new string[0], "B", true),
                new Job("3", 3, new string[]{ "2" }, "C", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(0, solution[1].StartTimeSec);
            Assert.AreEqual(1, solution[2].StartTimeSec);

            Assert.AreEqual(4, solution[2].FinishTimeSec);

            Assert.AreEqual(5, solution.TotalElapsedTime);
        }

    }
}
