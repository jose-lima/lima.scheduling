﻿using Lima.Scheduling.Core.V2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Tests {

    [TestClass]
    public class SchedulerV2 {

        [TestMethod]
        public void GetSolution_all_jobs_share_same_resource() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[0], "A", true),
                new Job("3", 5, new string[0], "A", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(1, solution[1].StartTimeSec);
            Assert.AreEqual(4, solution[2].StartTimeSec);

            Assert.AreEqual(9, solution.TotalElapsedTime);
        }

        [TestMethod]
        public void GetSolution_2_of_3_jobs_depend_on_same_resource() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[0], "B", true),
                new Job("3", 5, new string[0], "B", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(3, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(0, solution[1].StartTimeSec);
            Assert.AreEqual(3, solution[2].StartTimeSec);

            Assert.AreEqual(8, solution.TotalElapsedTime);
        }

        [TestMethod]
        public void GetSolution_2_sequences_of_jobs_sharing_2_resource() {

            var jobs = new[] {

                new Job("1", 1, new string[0], "A", true),
                new Job("2", 3, new string[0], "A", true),
                new Job("3", 5, new string[0], "B", true),
                new Job("4", 7, new string[0], "B", true),
            };

            var solutions = Scheduler.FindSolutions(jobs).ToList();

            //assert
            Assert.AreEqual(1, solutions.Count);

            var solution = solutions[0];

            Assert.AreEqual(4, solution.Count);
            Assert.AreEqual("1", solution[0].JobId);
            Assert.AreEqual("2", solution[1].JobId);
            Assert.AreEqual("3", solution[2].JobId);
            Assert.AreEqual("4", solution[3].JobId);

            Assert.AreEqual(0, solution[0].StartTimeSec);
            Assert.AreEqual(1, solution[1].StartTimeSec);
            Assert.AreEqual(0, solution[2].StartTimeSec);
            Assert.AreEqual(5, solution[3].StartTimeSec);

            Assert.AreEqual(12, solution.TotalElapsedTime);
        }
    }
}
