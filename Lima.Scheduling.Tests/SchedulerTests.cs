﻿using Lima.Scheduling.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace Lima.Scheduling.Tests {

    [TestClass]
    public partial class SchedulerTests {

        [TestMethod]
        public void Run_single_sequence_succeeds() {

            //setup resources
            var resources = new []{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
                new Resource(){ Id = "C" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, orderId: "1");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, preceedingJobIds: new[] { "1" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(300)), requiredResourceIds: new[] { "C" }, preceedingJobIds: new[] { "2" }, orderId: "3");

            //Run            
            scheduler.RunAllJobs();

            //Assert
            scheduler.WaitAllJobsFinished(TimeSpan.FromSeconds(3));
            Assert.AreEqual(3, scheduler.FinishedJobs.Select(x => x.JobId).Distinct().Count());
            Assert.AreEqual("1", scheduler.FinishedJobs[0].JobId);
            Assert.AreEqual("2", scheduler.FinishedJobs[1].JobId);
            Assert.AreEqual("3", scheduler.FinishedJobs[2].JobId);
            Console.WriteLine($"Total execution time: {scheduler.TotalExecutedDuration.TotalMilliseconds}ms");
            Assert.IsTrue(scheduler.TotalExecutedDuration < TimeSpan.FromMilliseconds(700));           
        }

        [TestMethod]
        public void Run_job_waits_multiple_preceeding_jobs() {

            //setup resources
            var resources = new[]{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
                new Resource(){ Id = "C" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, orderId: "1");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(300)), requiredResourceIds: new[] { "C" }, preceedingJobIds: new[] { "1", "2" }, orderId: "3");

            //Run            
            scheduler.RunAllJobs();

            //Assert
            scheduler.WaitAllJobsFinished(TimeSpan.FromSeconds(3));
            Assert.AreEqual(3, scheduler.FinishedJobs.Select(x => x.JobId).Distinct().Count());
            Assert.AreEqual("3", scheduler.FinishedJobs[2].JobId);
            Assert.IsTrue(scheduler.Jobs["3"].StartTime > scheduler.Jobs["1"].FinishTime);
            Assert.IsTrue(scheduler.Jobs["3"].StartTime > scheduler.Jobs["2"].FinishTime);
            Console.WriteLine($"Total execution time: {scheduler.TotalExecutedDuration.TotalMilliseconds}ms");
            Assert.IsTrue(scheduler.TotalExecutedDuration < TimeSpan.FromMilliseconds(550));
        }

        [TestMethod]
        [ExpectedException(typeof(NoRootJobsFoundException))]
        public void Run_all_jobs_with_dependencies_throws() {

            //setup resources
            var resources = new[]{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
                new Resource(){ Id = "C" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, preceedingJobIds: new[] { "3" }, orderId: "1", allowUnexistingJobs: true);
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, preceedingJobIds: new[] { "1" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(300)), requiredResourceIds: new[] { "C" }, preceedingJobIds: new[] { "2" }, orderId: "3");

            //Run            
            scheduler.RunAllJobs();        
        }

        [TestMethod]
        [ExpectedException(typeof(CircularDependencyException))]
        public void Run_circular_job_dependency_throws() {

            //setup resources
            var resources = new[]{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
                new Resource(){ Id = "C" },
                new Resource(){ Id = "D" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, preceedingJobIds: new[] { "3" }, orderId: "1", allowUnexistingJobs: true);
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, preceedingJobIds: new[] { "1" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(300)), requiredResourceIds: new[] { "C" }, preceedingJobIds: new[] { "2" }, orderId: "3");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(300)), requiredResourceIds: new[] { "D" }, orderId: "4");

            //Run            
            scheduler.RunAllJobs();
        }

        [TestMethod]
        public void Run_jobs_wait_for_resource_reservation_succeeds() {

            //setup resources
            var resources = new[]{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
                new Resource(){ Id = "C" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, orderId: "1");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(250)), requiredResourceIds: new[] { "A" }, orderId: "3");

            //run
            scheduler.RunAllJobs();

            //assert
            scheduler.WaitAllJobsFinished(TimeSpan.FromSeconds(3));
            Assert.AreEqual(3, scheduler.FinishedJobs.Select(x => x.JobId).Distinct().Count());

            // Jobs 1 and 3 depedend on same resource but order of start between them is not fixed,
            // so we have 2 options:
            // Job 1 started first
            if (scheduler.FinishedJobs[0].JobId == "1") {             
                Assert.IsTrue(scheduler.GetFinishedJob("3").TimeStamp.Ticks > scheduler.GetFinishedJob("1").TimeStamp.Ticks);
                Assert.IsTrue(scheduler.GetFinishedJob("3").TimeStamp.Ticks > scheduler.GetFinishedJob("2").TimeStamp.Ticks);
            }
            // Job 3 started first
            else {
                Assert.IsTrue(scheduler.GetFinishedJob("1").TimeStamp.Ticks > scheduler.GetFinishedJob("3").TimeStamp.Ticks);
                Assert.IsTrue(scheduler.GetFinishedJob("1").TimeStamp.Ticks > scheduler.GetFinishedJob("2").TimeStamp.Ticks);
            }

            Console.WriteLine($"Total execution time: {scheduler.TotalExecutedDuration.TotalMilliseconds}ms");
            Assert.IsTrue(scheduler.TotalExecutedDuration < TimeSpan.FromMilliseconds(400));
            
        }

        /// <summary>
        /// Use case: 1 job does not free a resource when finished.
        /// Other jobs need this resource. Scheduler must decide
        /// what jobs to execute first on the resource so that all
        /// jobs are executed at the end.
        /// </summary>
        [TestMethod] //TODO: red or green depending on which job processor decides to start first. Fix.
        public void Run_solve_sequence_with_one_resource_not_freed_by_job() {

            //setup resources
            var resources = new[]{
                new Resource(){ Id = "A" },
                new Resource(){ Id = "B" },
            };

            var scheduler = new Scheduler();
            scheduler.AddResources(resources);

            //setup jobs
            scheduler.LoadJob(new OccupyDelay(TimeSpan.FromMilliseconds(100)), requiredResourceIds: new[] { "A" }, orderId: "1"); //TODO: constrain this job to start first on the test.
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(200)), requiredResourceIds: new[] { "B" }, orderId: "2");
            scheduler.LoadJob(new OccupyDelayFree(TimeSpan.FromMilliseconds(250)), requiredResourceIds: new[] { "A" }, orderId: "3");

            //run
            scheduler.RunAllJobs();

            //assert
            scheduler.WaitAllJobsFinished(TimeSpan.FromSeconds(3));

            Assert.AreEqual(3, scheduler.FinishedJobs.Select(x => x.JobId).Distinct().Count());

            Assert.IsTrue(scheduler.Jobs["1"].StartTime > scheduler.Jobs["3"].FinishTime );

            Assert.IsTrue(scheduler.TotalExecutedDuration < TimeSpan.FromMilliseconds(400));
            Console.WriteLine($"Total execution time: {scheduler.TotalExecutedDuration}");
        }

        private void DelayAction() {

        }

        public abstract class JobPayload : IJobPayload {

            public JobPayload(TimeSpan estimatedDuration) {

                EstimatedDuration = estimatedDuration;
            }

            public TimeSpan EstimatedDuration { get; }

            public virtual string Name => GetType().Name;

            public abstract void Execute(params ResourceUseHandle[] reservedResources);
        }

        public class OccupyDelay : JobPayload {

            public OccupyDelay(TimeSpan estimatedDuration) : base(estimatedDuration) {
            }

            public override void Execute(params ResourceUseHandle[] reservedResources) {
                reservedResources.ToList().ForEach(x => x.Occupy());
                Thread.Sleep(EstimatedDuration);
            }
        }

        public class OccupyDelayFree: JobPayload {

            public OccupyDelayFree(TimeSpan estimatedDuration) : base(estimatedDuration) {
            }

            public override void Execute(params ResourceUseHandle[] reservedResources) {
                reservedResources.ToList().ForEach(x => x.Occupy());
                Thread.Sleep(EstimatedDuration);
                reservedResources.ToList().ForEach(x => x.Free());
            }
        }

    }
}
