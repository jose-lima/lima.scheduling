﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Lima.Scheduling.Wpf {

    public static class ViewFactory {

        public static Rectangle CreateJobView() {

            return new Rectangle() {
                Width = 100,
                Height = 300,
                Fill = Brushes.DodgerBlue,
                Margin = new Thickness(50, 50, 0, 0)
            };
        }
    }
}
