﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lima.Scheduling.Wpf {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public MainWindow() {

            InitializeComponent();

            var job1 = new Rectangle() {
                Width = 100,
                Height = 300,
                Fill = Brushes.DodgerBlue,
                Margin = new Thickness(50, 50, 0, 0)
            };

            this.GantChart.Children.Add(job1);

            var job2 = new Rectangle() {
                Width = 100,
                Height = 50,
                Fill = Brushes.IndianRed,
                Margin = new Thickness(50, 50 + job1.Height, 0, 0)
            };

            this.GantChart.Children.Add(job2);
        }
    }
}
