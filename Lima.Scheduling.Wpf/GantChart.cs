﻿using Lima.Scheduling.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Lima.Scheduling.Wpf {

    public class GantChart {

        private readonly Canvas _canvas;
        private Dictionary<string, Dictionary<string, IJob>> _jobSequences = 
            new Dictionary<string, Dictionary<string, IJob>>();


        public GantChart(Canvas canvas) {
            this._canvas = canvas;
        }

        public void AddJob(string sequenceId, IJob job) {

            if (_jobSequences.ContainsKey(sequenceId)) {
                _jobSequences.Add(sequenceId, new Dictionary<string, IJob>());
            }

            _jobSequences[sequenceId][job.Id] = job;
        }

        public void Redraw() {

            _canvas.Children.Clear();
            
            foreach(var sequence in _jobSequences.Values) {
                Draw(sequence);
            }
        }

        private void Draw(Dictionary<string, IJob> jobsSequence) {

            foreach (var job in jobsSequence.Values) {
                Draw(job);
            }
        }

        private void Draw(IJob job) {

            var jobView = new Rectangle() {
                Width = 100,
                Height = 300,
                Fill = Brushes.DodgerBlue,
                Margin = new Thickness(50, 50, 0, 0)
            };

            _canvas.Children.Add(jobView);
        }
    }
}
