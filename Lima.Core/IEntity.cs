﻿
namespace Lima.Core {

    public interface IEntity {

        string Id { get; }
    }
}
