﻿using System;

namespace Lima.Scheduling.Core {

    public class JobFinishedEventArgs: EventArgs {

        public JobFinishedEventArgs(string jobId, TimeSpan timeStamp) {
            JobId = jobId;
            TimeStamp = timeStamp;
        }

        public string JobId { get; }

        public TimeSpan TimeStamp { get; }
    }
}
