﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core {

    public class JobsSequence {

        public string Id { get; set; }

        public IEnumerable<JobProcessor> Jobs = new List<JobProcessor>();

    }
}
