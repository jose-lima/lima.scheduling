﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core.V2 {

    public class Solution {

        private IList<ScheduledJob> _scheduledJobs;

        public Solution(IEnumerable<ScheduledJob> scheduledJobs) {
            _scheduledJobs = scheduledJobs.ToList();
        }

        public int Count => _scheduledJobs.Count;

        public ScheduledJob this[int index] => _scheduledJobs[index];

        public int TotalElapsedTime => _scheduledJobs.Max(x => x.FinishTimeSec.Value);

    }

    public class ScheduledJob {

        public ScheduledJob(Job job, int? startTimeSec = null, int? finishTimeSec = null)
            : this(job.Id, job.RequiredResource, startTimeSec, finishTimeSec) {

        }

        public ScheduledJob(string jobId, string resourceId, int? startTimeSec = null, int? finishTimeSec = null) {
            JobId = jobId;
            ResourceId = resourceId;
            StartTimeSec = startTimeSec;
            FinishTimeSec = finishTimeSec;
        }

        public string ResourceId { get; }

        /// <summary>
        /// Note: Job duration is 1-1 to the scheduled resource time.
        /// Jobs sequentially using different resources  different jobs.
        /// </summary>
        public string JobId { get; }

        public int? StartTimeSec { get; set; }

        public int? FinishTimeSec { get; set; }
    }

    public class Job {

        public Job(string id,
            int estimatedDurationSec,
            IEnumerable<string> requiredJobs,
            string requiredResource,
            bool freesResourceAtEnd) {

            Id = id;
            EstimatedDurationSec = estimatedDurationSec;
            RequiredJobs = requiredJobs.ToList();
            RequiredResource = requiredResource;
            FreesResourceAtEnd = freesResourceAtEnd;
        }

        public string Id { get; }

        public int EstimatedDurationSec { get; }

        public List<string> RequiredJobs { get; }

        public string RequiredResource { get; }

        public bool FreesResourceAtEnd { get; }
    }
}
