﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core.V2 {

    public class Scheduler {

        public static IEnumerable<Solution> FindSolutions(Job[] jobs) {

            var jobsDic = jobs.ToDictionary(x => x.Id, x => x);

            var scheduledJobs = new List<ScheduledJob>();

            foreach (var job in jobs) {
                ScheduleJob(job, jobsDic, scheduledJobs);
            }

            return new[] { new Solution(scheduledJobs) };
        }

        public static ScheduledJob ScheduleJob(Job job,
            Dictionary<string, Job> jobsDic,
            List<ScheduledJob> scheduledJobs) {

            ScheduledJob scheduledJob;
            int startTime;
            int finishTime;

            //Add depedency on jobs scheduled for the required resource
            var scheduledJobsForRequiredResource =
                scheduledJobs.Where(x => x.ResourceId == job.RequiredResource);
            job.RequiredJobs.AddRange(scheduledJobsForRequiredResource.Select(x => x.JobId));

            //Independent job
            if (!job.RequiredJobs.Any()) {

                startTime = 0;
                finishTime = startTime + job.EstimatedDurationSec;

                //Schedule resource
                scheduledJob = new ScheduledJob(job.Id, job.RequiredResource, startTime, finishTime);
                scheduledJobs.Add(scheduledJob);

                return scheduledJob;
               
            }

            //Dependent job
            var potentialStartTimes = new List<int>();

            foreach (var requiredJobId in job.RequiredJobs) {

                //required job already scheduled
                var requiredScheduledJob = scheduledJobs.SingleOrDefault(x => x.JobId == requiredJobId);
                if (requiredScheduledJob != null && requiredScheduledJob.FinishTimeSec != null) {

                    potentialStartTimes.Add(requiredScheduledJob.FinishTimeSec.Value);
                    continue;
                }

                //required job not yet scheduled
                var requiredJob = jobsDic[requiredJobId];
                requiredScheduledJob = ScheduleJob(requiredJob, jobsDic, scheduledJobs);
                potentialStartTimes.Add(requiredScheduledJob.FinishTimeSec.Value);
            }

            startTime = potentialStartTimes.Max();
            scheduledJob = new ScheduledJob(job, startTime, startTime + job.EstimatedDurationSec);
            scheduledJobs.Add(scheduledJob);
            return scheduledJob;

        }
    }
}
