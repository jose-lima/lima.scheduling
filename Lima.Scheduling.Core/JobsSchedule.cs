﻿using Lima.Graphs;
using System.Collections.Generic;
using System.Linq;

namespace Lima.Scheduling.Core {

    public class JobsSchedule {

        private Graph _jobsGraph;

        public JobsSchedule(IReadOnlyDictionary<string, JobProcessor> jobs) {

            _jobsGraph = Graph.CreateGraph(jobs);

            if (!_jobsGraph.RootNodes.Any()) {
                throw new NoRootJobsFoundException("Circular job dependency found: there is no root job to start with, all jobs have pre-requisite jobs.");
            }

            var circularDependencyEdges = _jobsGraph.GetCircularDependencies();

            if (circularDependencyEdges.Any()) {
                throw new CircularDependencyException($"Circular job dependency found: {ToString(circularDependencyEdges)}.");
            }
        }

        private string ToString(IEnumerable<(string, string)?> circularDependencyEdges) {
            return $"{{ {string.Join(", ", circularDependencyEdges.Select(x => ToString(x)))} }}";
        }

        private string ToString((string, string)? edge) {
            return $"{edge.Value.Item1} -> {edge.Value.Item2}";
        }
    }

}
