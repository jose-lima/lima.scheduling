﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core {

    public interface IJob {

        string Id { get; }

        string Label { get; }

    }
}
