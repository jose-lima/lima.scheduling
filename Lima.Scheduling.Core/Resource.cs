﻿using System;
using System.Threading;

namespace Lima.Scheduling.Core {

    public class Resource {

        public Resource() {

        }

        public string Id { get; set; }

        public bool IsOccupied { get; internal set; }

        public override string ToString() {
            return $"{nameof(Resource)} {Id}";
        }

    }
}
