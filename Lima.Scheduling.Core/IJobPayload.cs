﻿using System;

namespace Lima.Scheduling.Core {

    public interface IJobPayload {

        /// <summary>
        /// Non-unique name.
        /// </summary>
        string Name { get; }

        void Execute(params ResourceUseHandle[] reservedResources);

        TimeSpan EstimatedDuration { get; }

    }
}
