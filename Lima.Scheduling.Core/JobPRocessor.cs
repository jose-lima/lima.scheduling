﻿using Lima.Core;
using Lima.Graphs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core {

    /// <summary>
    /// Scheduler job processor responsible for managing the execution
    /// of <see cref="IJobPayload"/>'s (consumer application jobs).
    /// </summary>
    public class JobProcessor: IEntity, IFanInNode {

        private readonly Task _task;
        private readonly string _timeStampFormat = "HH:mm:ss.fff";
        private readonly TimeSpan? _waitRequirementTimeout = TimeSpan.FromSeconds(3);
        private readonly Scheduler _scheduler;
        private readonly Stopwatch _durationWatch = new Stopwatch();
        private readonly IJobPayload _jobPayload;

        internal JobProcessor(Scheduler scheduler, 
            IJobPayload jobPayload,
            IEnumerable<string> requiredResourceIds,
            IEnumerable<string> requiredJobIds = null,
            string orderId = null ) {

            Contract.Requires(requiredResourceIds != null && requiredResourceIds.Any(), "Jobs cannot exist without resources.");

            _scheduler = scheduler;           
            _jobPayload = jobPayload;
            OrderId = orderId ?? Guid.NewGuid().ToString();
            PreceedingJobIds = requiredJobIds ?? new List<string>(); //TODO: support multiple.
            RequiredResourceIds = requiredResourceIds; //TODO: support multiple.

            if (Debugger.IsAttached) {
                _waitRequirementTimeout = null;
            }

            //Config task
            _task = new Task(() => {

                WaitOnPreceedingJobs();
                var reservedResources = WaitOnRequiredResources();
                ExecuteJob(reservedResources);
                ReleaseResources(reservedResources);
                Finished?.BeginInvoke(this, new JobFinishedEventArgs(this.OrderId, _scheduler.RunTimer.Elapsed), null, null);

            });
        }

        /// <summary>
        /// Unique ID used for ordering job executions.
        /// </summary>
        public string OrderId { get; }

        public string Id => OrderId;

        public TimeSpan EstimatedDuration => _jobPayload.EstimatedDuration;

        public TimeSpan StartTime { get; private set; }

        public TimeSpan FinishTime { get; private set; }

        public TimeSpan ElapsedTime => _durationWatch.Elapsed;

        public IEnumerable<string> PreceedingJobIds { get; }

        public List<string> FanIn => PreceedingJobIds.ToList();

        public IEnumerable<string> RequiredResourceIds { get; }

        public TaskStatus Status => _task.Status;

        public bool IsRunning => Status == TaskStatus.Running;

        public Exception Exception => _task?.Exception;

        public object PayloadName => _jobPayload.Name;

        public EventHandler<JobFinishedEventArgs> Finished;

        internal void Run() {

            _task.Start();
        }

        private void ExecuteJob(IEnumerable<ResourceUseHandle> reservedResources) {

            LogAction($"{nameof(ExecuteJob)} {this.OrderId}", () => {

                StartTime = _scheduler.RunTimer.Elapsed;
                _durationWatch.Start();
                _jobPayload.Execute(reservedResources.ToArray());
                _durationWatch.Stop();
                FinishTime = _scheduler.RunTimer.Elapsed;

            });
        }

        private IEnumerable<ResourceUseHandle> WaitOnRequiredResources() {

            var reservedResources = new List<ResourceUseHandle>();

            foreach(var resource in RequiredResourceIds) {
                reservedResources.Add(WaitOnRequiredResource(resource));
            }

            return reservedResources;
        }

        private ResourceUseHandle WaitOnRequiredResource(string resourceId) {

            if (RequiredResourceIds == null) {
                return null;
            }

            _scheduler.ResourceReservationHandles.TryGetValue(resourceId, out ResourceReservationHandle resourceReservationHandle);

            if (resourceReservationHandle == null) {
                throw new Exception($"'{this.ToString()}' failed to find reservation handle for required resource '{RequiredResourceIds}'.");
            }

            ResourceUseHandle reservedResource = default;

            LogAction($"{nameof(WaitOnRequiredResource)} {ToString(RequiredResourceIds)}", () => {
                reservedResource = resourceReservationHandle.Reserve(_waitRequirementTimeout);
            });

            return reservedResource;
        }

        internal bool Wait(TimeSpan? timeout = null) {

            if (timeout == null) {
                _task.Wait();
                return true;
            }

            return _task.Wait(timeout.Value);
        }
                

        private void WaitOnPreceedingJobs() {

            if (!PreceedingJobIds.Any()) {
                return;
            }

            foreach (var job in PreceedingJobIds) {
                WaitOnPreceedingJob(job);
            }           
        }

        private void WaitOnPreceedingJob(string preceedingJobId) {

            if (PreceedingJobIds == null) {
                return;
            }

            var runningPreceedingJob = _scheduler.Jobs.Values.SingleOrDefault(x => x.OrderId == preceedingJobId);

            if (runningPreceedingJob == null) {
                throw new Exception($"'{this.ToString()}' failed to find preceeding job '{ToString(PreceedingJobIds)}'.");
            }

            LogAction($"{nameof(WaitOnPreceedingJob)} {preceedingJobId}", () => {

                if (!runningPreceedingJob.Wait(_waitRequirementTimeout)) {
                    throw new TimeoutException($"Job {this.ToString()} timed out waiting on job '{preceedingJobId}'.");    
                }
            });

        }

        private void ReleaseResources(IEnumerable<ResourceUseHandle> reservedResources) {

            foreach (var resource in reservedResources) {
                ReleaseResource(resource);
            }
        }

        private void ReleaseResource(ResourceUseHandle reservedResource) {          
            _scheduler.RealeaseResource(reservedResource.ResourceId);
        }

        private string GetTimeStamp() {
            return $"{_scheduler.RunTimer.ElapsedMilliseconds.ToString()}ms";
        }

        private void LogAction(string actionName, Action action) {

            Console.WriteLine($"{ GetTimeStamp() } \t| { this.ToString()} \t| { actionName } \t| begin.");
            action.Invoke();
            Console.WriteLine($"{ GetTimeStamp() } \t| { this.ToString()} \t| { actionName } \t| end.");
        }

        public override string ToString() {
            return $"{nameof(JobProcessor)} {OrderId}";
        }

        private string ToString(IEnumerable<object> collection) {
            return $"{{ {string.Join(", ", collection.Select(x => x.ToString()))} }}";
        }

    }
}
