﻿using System;
using System.Threading;

namespace Lima.Scheduling.Core {

    public class ResourceReservationHandle {

        private Resource _resource;

        public ResourceReservationHandle(Resource resource) {
            _resource = resource;
        }

        public string ResourceId => _resource.Id;

        private Mutex Reservation { get; set; } = new Mutex();

        public void Release() {
            Reservation.ReleaseMutex();
        }

        public ResourceUseHandle Reserve(TimeSpan? timeout) {

            var milliseconds = (int) (timeout?.TotalMilliseconds ?? -1);

            if (Reservation.WaitOne(milliseconds)) {
                return new ResourceUseHandle(_resource);
            }

            throw new TimeoutException($"Timeout of {milliseconds}ms exceeded waiting on resource {ResourceId}.");
        }
    }
}
