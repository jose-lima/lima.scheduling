﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.Scheduling.Core {

    public class ResourceUseHandle {

        private Resource _resource;

        public ResourceUseHandle(Resource resource) {
            _resource = resource;
        }

        public string ResourceId => _resource.Id;

        public void Occupy() {
            if (_resource.IsOccupied) {
                throw new InvalidOperationException($"Tried to occupy occupied resource {ResourceId}.");
            }
            _resource.IsOccupied = true;
        }

        public void Free() {
            _resource.IsOccupied = false;
        }
    }
}
