﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Diagnostics;

namespace Lima.Scheduling.Core {

    public class Scheduler {

        private readonly ManualResetEventSlim _allJobsFinished = new ManualResetEventSlim();
        private readonly Dictionary<string, JobProcessor> _jobProcessors = new Dictionary<string, JobProcessor>();
        internal readonly Dictionary<string, ResourceReservationHandle> ResourceReservationHandles =
            new Dictionary<string, ResourceReservationHandle>();
        private readonly List<JobFinishedEventArgs> _finishedJobs = new List<JobFinishedEventArgs>();
        internal IEnumerable<JobProcessor> RunningJobs => _jobProcessors.Values.Where(x => x.IsRunning);
        public readonly Dictionary<string, Resource> Resources = new Dictionary<string, Resource>();
        public EventHandler<JobFinishedEventArgs> JobFinished;
        public IReadOnlyList<JobFinishedEventArgs> FinishedJobs => _finishedJobs.AsReadOnly();
        public IReadOnlyDictionary<string, JobProcessor> Jobs => _jobProcessors.ToImmutableDictionary();

        public readonly Stopwatch RunTimer = new Stopwatch(); 

        public void AddResources(IEnumerable<Resource> resources) {
            resources.ToList().ForEach(x => AddResource(x));
        }

        public void AddResource(Resource resource) {

            if (ResourceReservationHandles.ContainsKey(resource.Id)) {
                throw new ArgumentException($"There exist already a resource with id {resource.Id}.");
            }

            ResourceReservationHandles[resource.Id] = new ResourceReservationHandle(resource);
        }

        /// <summary>
        /// Load a job into scheduler without starting it.
        /// </summary>
        /// <param name="jobPayload">The external or consuming application job.</param>
        /// <param name="requiredResourceIds">Resource on which current job depend on.</param>
        /// <param name="preceedingJobIds">Jobs that must be finished in order for current job to start.</param>
        /// <param name="allowUnexistingJobs">Allow dependency on jobs that haven't been loaded yet.</param>
        public void LoadJob(IJobPayload jobPayload,
            IEnumerable<string> requiredResourceIds,
            IEnumerable<string> preceedingJobIds = null,
            string orderId = null,
            bool allowUnexistingJobs = false) {

            Contract.Requires(jobPayload != null, $"{nameof(jobPayload)} cannot be null.");
            Contract.Requires(requiredResourceIds != null, $"{nameof(requiredResourceIds)} cannot be null.");

            if (!allowUnexistingJobs && preceedingJobIds != null) {
                foreach (var preceedingJobId in preceedingJobIds) {
                    if (preceedingJobId != null && !_jobProcessors.ContainsKey(preceedingJobId)) {
                        throw new ArgumentException($"There exist no preceeding job with order id {preceedingJobId}.");
                    }
                }
            }

            if (orderId != null && _jobProcessors.ContainsKey(orderId)) {
                throw new ArgumentException($"There exist already a job with order id {orderId}.");
            }

            var jobProcessor = new JobProcessor(this, jobPayload, requiredResourceIds, preceedingJobIds, orderId);
            jobProcessor.Finished += OnJobFinished;
            _jobProcessors[jobProcessor.OrderId] = jobProcessor;
        }

        public void RunAllJobs(TimeSpan? totalRunTimeout = null) {

            RunTimer.Restart();

            var jobPlan = new JobsSchedule(_jobProcessors);

            Parallel.ForEach(_jobProcessors.Values, x => x.Run());

            Parallel.ForEach(RunningJobs, x => x.Wait(totalRunTimeout));

            var jobExceptions = RunningJobs
                .Where(x => x.Exception != null)
                .Select(x => new Exception($"Exception on Job '{x.PayloadName} {x.OrderId}'.", x.Exception));

            if (jobExceptions.Any()) {
                throw new AggregateException("One or more jobs thrown exceptions.", jobExceptions);
            }
        }

        public void WaitAllJobsFinished(TimeSpan? timeout = null) {

            var milliseconds = timeout?.TotalMilliseconds ?? -1;

            if (!_allJobsFinished.Wait((int)milliseconds)) {
                throw new TimeoutException("Timed out waiting for all jobs to finish.");
            }
        }

        public TimeSpan TotalExecutedDuration {
            get {
                var firstJob = _jobProcessors[_finishedJobs.First().JobId];
                var lastJob = _jobProcessors[_finishedJobs.Last().JobId];
                return lastJob.FinishTime - firstJob.StartTime;
            }
        }

        public JobFinishedEventArgs GetFinishedJob(string id) {
            return _finishedJobs.SingleOrDefault(x => x.JobId == id);
        }

        private void OnJobFinished(object sender, JobFinishedEventArgs args) {
            _finishedJobs.Add(args);
            JobFinished?.Invoke(this, args);

            if (Jobs.All(x => 
                x.Value.Status == TaskStatus.RanToCompletion 
                || x.Value.Status == TaskStatus.Faulted
                || x.Value.Status == TaskStatus.Canceled)) {

                _allJobsFinished.Set();
            }
        }

        internal void RealeaseResource(string resourceId) {
            ResourceReservationHandles[resourceId].Release();
        }
    }
}
