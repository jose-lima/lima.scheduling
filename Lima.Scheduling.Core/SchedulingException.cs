﻿using System;

namespace Lima.Scheduling.Core {

    public class SchedulingException: Exception {

        public SchedulingException(string msg) : base(msg) {
        }
    }

    public class CircularDependencyException : SchedulingException {

        public CircularDependencyException(string msg): base(msg) {
        }
    }

    public class NoRootJobsFoundException : SchedulingException {

        public NoRootJobsFoundException(string msg) : base(msg) {
        }
    }
}
