﻿using Lima.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Lima.Graphs {

    public class Graph {

        private Dictionary<string, BiDiNode> _graph;

        private Graph(Dictionary<string, BiDiNode> dictionary) {
            _graph = dictionary;
        }

        public IEnumerable<BiDiNode> RootNodes => _graph.Values.Where(x => !x.FanIn.Any());

        public static Graph CreateGraph<TContent>(IEnumerable<TContent> entities)
            where TContent : IEntity, IFanInNode {

            var nodeDic = entities.ToDictionary(x => x.Id, x => x);

            return CreateGraph(new ReadOnlyDictionary<string, TContent>(nodeDic));
        }

        public static Graph CreateGraph<TContent>(IReadOnlyDictionary<string, TContent> entities)
            where TContent : IEntity, IFanInNode {

            var dic = CreateGraphRecurr(entities);
            return new Graph(dic);
        }

        private static Dictionary<string, BiDiNode> CreateGraphRecurr<TContent>(
            IReadOnlyDictionary<string, TContent> entities, Dictionary<string, BiDiNode> graph = null)
            where TContent: IEntity, IFanInNode {

            if (graph == null) {
                graph = new Dictionary<string, BiDiNode>();
            }

            foreach (var entity in entities.Values) {

                AddNodeIfNew(graph, entity);
                var currentNode = graph[entity.Id];

                foreach (var preceedingJob in entity.FanIn.Select(x => entities[x])) {
                    AddNodeIfNew(graph, preceedingJob);
                    graph[preceedingJob.Id].FanOut.Add(currentNode.Id);
                }
            }

            return graph;
        }

        /// <summary>
        /// Returns edge causing circular dependency: (sourceId, targetId).
        /// </summary>
        public IEnumerable<(string, string)?> GetCircularDependencies() {

            var closingEdges = new List<(string, string)?>();

            foreach (var node in _graph.Values) {

                var closingEdge = CheckCircularJobDependencies(node);

                if(closingEdge != null) {
                    closingEdges.Add(closingEdge);
                }

            }

            return closingEdges;
        }

        private static void AddNodeIfNew<TContent>(Dictionary<string, BiDiNode> jobsGraph, TContent entity) where TContent : IEntity, IFanInNode {

            if (!jobsGraph.ContainsKey(entity.Id)) {

                var newNode = new BiDiNode(entity.Id) {
                    FanIn = entity.FanIn?.ToList() ?? new List<string>()
                };
                jobsGraph[entity.Id] = newNode;
            }
        }

        private (string, string)? CheckCircularJobDependencies(BiDiNode currentNode, List<string> currentPath = null) {

            if (currentPath == null) {
                currentPath = new List<string>();
            }

            if (currentPath.Contains(currentNode.Id)) {
                return (currentPath.Last(), currentNode.Id);
            }

            currentPath.Add(currentNode.Id);

            foreach (var target in currentNode.FanOut) {

                var result = CheckCircularJobDependencies(_graph[target], currentPath);

                if (result?.Item1 != null) {
                    return result;
                }

                else if (result?.Item1 == null && result?.Item2 != null) {
                    return (currentNode.Id, result.Value.Item2);
                }
            }

            currentPath.Remove(currentNode.Id);

            return null;
        }
    }

    public class BiDiNode: IFanInNode {

        public BiDiNode(string id) {
            Id = id;
        }

        public string Id { get; }

        public List<string> FanOut { get; set; } = new List<string>();

        public List<string> FanIn { get; set; } = new List<string>();
    }

    public interface IFanInNode {

        List<string> FanIn { get; }
    }
}
