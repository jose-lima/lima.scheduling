﻿using System;

namespace Lima.LiquidHandling {

    public class GrippingArm : LabwareHolder {

        public GrippingArm(string id = null) : base(id) {
        }

        public void MoveLabware(ILabwareHolder source, ILabwareHolder target) {

            if (source.CurrentLabware == null) {
                throw new ArgumentException($"Source labware holder {source.Id} contains currently no labware.");
            }

            if (target.CurrentLabware != null) {
                throw new ArgumentException($"Target labware holder {target.Id} contains currently labware {target.CurrentLabware.Id}.");
            }

            var labware = source.TakeLabware();
            target.Place(source.CurrentLabware);
        }
    }
}
