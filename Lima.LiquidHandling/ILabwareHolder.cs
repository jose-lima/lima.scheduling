﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lima.LiquidHandling {

    public interface ILabwareHolder {

        string Id { get; }

        Labware CurrentLabware { get; }

        void Place(Labware labware);

        Labware TakeLabware();

        bool IsFree { get; }
    }
}
