﻿using System.Threading;

namespace Lima.LiquidHandling {

    public class Shaker: LabwareHolder
    {
        public Shaker(string id = null) : base(id) {
        }

        public void Shake(int timeMs) {
            Thread.Sleep(timeMs);
        }
    }
}
