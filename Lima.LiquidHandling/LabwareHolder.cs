﻿
using System;

namespace Lima.LiquidHandling {

    public abstract class LabwareHolder : ILabwareHolder {

        public LabwareHolder(string id = null) {
            Id = id ?? Guid.NewGuid().ToString();
        }

        public string Id { get; }

        public Labware CurrentLabware { get; private set; }

        public bool IsFree => CurrentLabware != null;

        public void Place(Labware labware) {
            CurrentLabware = labware;
        }

        public Labware TakeLabware() {
            var labware = CurrentLabware;
            CurrentLabware = null;
            return labware;
        }

        public override string ToString() {
            return $"{GetType().Name} {Id}";
        }
    }
}
